#!/bin/bash

sudo add-apt-repository ppa:saiarcot895/myppa
sudo apt-get update

sudo apt-get install apt-fast

for each_package in $(cat list); do
  echo "[x] Installing $each_package"
  sudo apt-fast -y install $each_package
done
echo "[x] All packages installed."

echo "[x] Installing carlhuda janus"
curl -L https://bit.ly/janus-bootstrap | bash
echo 'color xoria256'  >> ~/.vimrc.after

echo "[x] Creating repos directory"
mkdir ~/repos
cd ~/repos

echo "[x] Generating ssh keys"
ssh-keygen

echo "[x] Getting capstone"
wget https://pypi.python.org/packages/source/c/capstone/capstone-3.0.4.tar.gz#md5=3437fa632c89531a7d800a3f8c04fbe3
tar -xf capstone-3.0.4.tar.gz
cd capstone-3.0.4
python setup.py build
sudo python setup.py install
cd ~/repos

echo "[x] Cloning binjitsu"
git clone http://github.com/binjitsu/binjitsu.git
cd binjitsu
python setup.py build
sudo python setup.py install
cd ~/repos

echo "[x] Cloning radare2"
git clone http://github.com/radare/radare2.git

echo "[x] Getting pin"
wget http://software.intel.com/sites/landingpage/pintool/downloads/pin-2.14-71313-gcc.4.4.7-linux.tar.gz

echo "[x] Getting peda"
git clone http://github.com/longld/peda.git

echo "[x] Getting oh-my-zsh"
sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"


